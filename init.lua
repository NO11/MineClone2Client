local modname = minetest.get_current_modname()
local modpath = minetest.get_modpath(modname)

for _, f in ipairs({"sprint"}) do
	dofile(modpath .. "/" .. f .. ".lua")
end
